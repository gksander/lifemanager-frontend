/**
 * 
 * @param {Date} dateString 
 * See https://github.com/date-fns/date-fns/issues/376
 */

export default function (dateString = Date.now()) {
  const date = new Date(dateString);

  return new Date(
    date.getUTCFullYear(),
    date.getUTCMonth(),
    date.getUTCDate(),
    date.getUTCHours(),
    date.getUTCMinutes(),
    date.getUTCSeconds(),
  );
}