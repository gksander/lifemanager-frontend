import { format, isToday, isTomorrow } from 'date-fns'




/**
 * 
 * @param {Date} dateString 
 * See https://github.com/date-fns/date-fns/issues/376
 */
export const getUTCDate =  (dateString = Date.now()) => {
  const date = new Date(dateString);
  return new Date(
    date.getUTCFullYear(),
    date.getUTCMonth(),
    date.getUTCDate(),
    date.getUTCHours(),
    date.getUTCMinutes(),
    date.getUTCSeconds(),
  );
}


/**
 * Pretty display dates
 */
export const dateFilter = date => {
  // date = getUTCDate(date);
  if (isToday(date)) return "Today";
  else if (isTomorrow(date)) return "Tomorrow";
  else return format(date, `dddd MMM D, YYYY`)
}


/**
 * Function to sort by data
 */
export const sortTodosByDate = (a, b) => {
  if (a.due_date < b.due_date) return -1;
  else if (a.due_date > b.due_date) return 1;
  else return 0;
}


/**
 * Check that value is not empty
 */
export const checkNotBlank = v => !!v || 'This field is required.';