export default [
  // Default
  {
    id: 'default',
    title: 'Default',
    mode: 'dark',
    bg: '#395038',
    primary: '#dbe290',
    success: '#4caf50',
    info: '#2196f3',
    warning: '#fb8c00',
    error: '#ff5252'
  },

  // Dark
  {
    id: 'dark',
    title: 'Dark',
    mode: 'dark',
    bg: '#424242',
    primary: '#a2edff',
    success: '#4caf50',
    info: '#2196f3',
    warning: '#fb8c00',
    error: '#ff5252'
  },

  // Light
  {
    id: 'light',
    title: 'Light',
    mode: 'light',
    bg: '#fafafa',
    primary: '#4527a0',
    success: '#4caf50',
    info: '#2196f3',
    warning: '#fb8c00',
    error: '#ff5252'
  },


  // Purple
  {
    id: 'purple',
    title: 'Purple',
    mode: 'dark',
    bg: '#4527a0',
    primary: '#f06292',
    success: '#4caf50',
    info: '#2196f3',
    warning: '#fb8c00',
    error: '#ff5252'
  },

  // Sunburts
  {
    id: 'sunburst',
    title: "Sunburst",
    mode: 'dark',
    bg: '#533E2D',
    primary: '#DDCA7D',

    success: '#4caf50',
    info: '#2196f3',
    warning: '#fb8c00',
    error: '#ff5252'
  }
]