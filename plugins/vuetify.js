import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from "vuetify/es5/util/colors";

import '~/assets/style/app.styl'

// import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'fa',
  theme: {
    bg: '#395038',
    primary: '#dbe290',
    success: '#4caf50',
    info: '#2196f3',
    warning: '#fb8c00',
    error: '#ff5252'
  }
})