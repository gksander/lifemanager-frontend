import { VueEditor } from "vue2-editor";
import Vue from 'vue';

Vue.component('vue-editor', VueEditor);