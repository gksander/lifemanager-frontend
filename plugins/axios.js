export default function ({ $axios, app }) {
  $axios.onRequest((config) => {
    let token;
    if (localStorage) token = localStorage.getItem('authToken');
    if (token) {
      config.headers.common['Authorization'] = `Bearer ${token}`;
    }
  })
 }