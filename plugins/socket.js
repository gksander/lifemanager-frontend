import io from 'socket.io-client';
import Vue from 'vue';

const socket = io(process.env.API_BASE);

Vue.prototype.$socket = socket;