export let storeTitle = (title) => ({
  mounted () {
    this.$store.commit('SET_PAGE_TITLE', title)
  },
  destroyed () {
    this.$store.commit('SET_PAGE_TITLE', 'Avocalendar')
  }
})