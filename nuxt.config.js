const pkg = require('./package');
require("dotenv").config();
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

// Variables
let isProduction = process.env.NODE_ENV == 'production';
const API_BASE = isProduction ? 'https://api.gksander.com' : "http://192.168.0.24:1337";
const SITE_BASE = isProduction ? 'https://life.gksander.com' : "http://192.168.0.24:3000";

module.exports = {
  mode: 'universal',
  
  // srcDir: 'client/',
  // generate: {
  //   dir: 'dist/'
  // },

  env: {
    API_BASE: API_BASE,
    SITE_BASE: SITE_BASE
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    script: [
      // {src: "https://cdn.quilljs.com/1.3.6/quill.js"}
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.3/css/all.css' },
      // {rel: "stylesheet", href: "https://cdn.quilljs.com/1.3.6/quill.snow.css"},
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    './node_modules/animate.css/animate.min.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    '@/plugins/axios',
    '@/plugins/socket',
    {src: '@/plugins/editor', ssr: false}
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // '@nuxtjs/onesignal',
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: API_BASE,
    browserBaseURL: API_BASE,
  },

  // Onesignal Configuration
  // oneSignal: {
  //   init: {
  //     appId: '6395d9f3-9344-42ef-a317-73e0eb7a93c5',
  //     allowLocalhostAsSecureOrigin: true,
  //     welcomeNotification: {
  //       disable: true
  //     }
  //   }
  // },

  // Manifest for PWA
  manifest: {
    name: 'Avocalendar',
    short_name: 'Avocalendar',
    lang: 'en',
    display: "standalone",
    background_color: "#395038",
    description: "Collaborative to-do's and calendars",
    mobileAppIOS: true,
    start_url: "/",
    display: "standalone"
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: [/^vuetify/],
    
    babel: {
      plugins: [
        ['transform-imports', {
          'vuetify': {
            'transform': 'vuetify/es5/components/${member}',
            'preventFullImport': true
          }
        }]
      ]
    },

    plugins: [
      new VuetifyLoaderPlugin()
    ]
  }
}
