// import io from 'socket.io-client';
// let socket;
import THEMES from '~/assets/js/THEMES'


const bare_user = {
  _id: "",
  email: "",
  fullname: "",
  theme: THEMES[0],
  personalNote: ""
}


export const state = () => ({
  user: bare_user,
  member_todolists: [],
  member_calendars: [],
  member_notelists: [],
  member_lists: [],


  // Snackbar
  snackbar: {
    message: "",
    type: "success"
  },

  loadingMessage: "Please stand by.",
  isLoading: false,

  title: ""

})


/** ------------------------------------
 * Getters
------------------------------------- */
export const getters = {
  // Check if logged in
  isLoggedIn: state => {
    return state.user._id != "";
  },

  userTheme: state => {
    let theme = state.user.theme;
    if (theme.id) {
      return THEMES.find(t => t.id == theme.id);
    } else {
      return THEMES.find(t => t.id == 'default');
    }
    // return {
    //   id: theme.id || 'default',
    //   mode: theme.mode || 'dark',
    //   primary: theme.primary || bare_user.theme.primary,
    //   dark: theme.dark || bare_user.theme.dark,
    //   bg: theme.bg || bare_user.theme.bg,
    //   success: theme.success || bare_user.theme.success,
    //   info: theme.info || bare_user.theme.info,
    //   warning: theme.warning || bare_user.theme.warning,
    //   error: theme.error || bare_user.theme.error,
    //   cardTitleColor: theme.cardTitle || bare_user.theme.cardTitle,
    // }
  },

  /**
   * All of a user's todos
   */
  allTodos: state => {
    let todos = [];
    for (let list of state.member_todolists) {
      for (let todo of list.todos) {
        todos.push({...todo, color: list.color || '#ffffff', list: list.title})
      }
    }
    return todos;
  },

  /**
   * All of a user's calendar items
   */
  allCalendarItems: state => {
    let items = [];
    for (let calendar of state.member_calendars) {
      for (let item of calendar.calendaritems) {
        items.push({...item, calendar_title: calendar.title});
      }
    }
    return items;
  }
}



/** ------------------------------------
 * Mutations
------------------------------------- */
export const mutations = {

  SET_PAGE_TITLE(state, title) {
    state.title = title;
  },

  /**
   * Update user.
   */
  updateUser(state, user) {
    // If no data passed, wipe user data
    if (user == null) {
      state.user = bare_user;
      return;
    }

    // Update only appropriate user fields
    let { _id, email, fullname, theme, personalNote } = user;
    state.user._id = _id || bare_user._id;
    state.user.email = email || bare_user.email;
    state.user.fullname = fullname || bare_user.fullname;
    state.user.theme = theme || bare_user.theme;
    state.user.personalNote = personalNote || bare_user.personalNote;
    // state.user = { _id, email, fullname, theme };
    // Store user data in localStorage for quick retrieval.
    if (process.browser) {
      localStorage.setItem('user', JSON.stringify(state.user));
    }
  },

  updateStateProperty(state, payload) {
    state[payload.property] = payload.value;
  },

  snackbar(state, payload) {
    if (payload) {
      state.snackbar = payload;
    } else {
      state.snackbar = {
        message: "",
        type: "success"
      }
    }
  },

  isLoading(state, payload) {
    let defaultMessage = "Please stand by.";
    if (payload === true) {
      state.loadingMessage = defaultMessage;
      state.isLoading = true;
    } else if (payload === false) {
      state.isLoading = false;
    }
    else { // Custom
      state.loadingMessage = payload.message;
      state.isLoading = payload.value;
    }
  },

  // Update personal note
  updatePersonalNote(state, note) {
    state.user.personalNote = note;
  },

  // Update a todo
  listitem_upsert(state, listitem) {
    // Try to find the todo list that the todo belongs to.
    let list = state.member_lists.find(list => list._id == listitem.list);
    if (!list) return;

    let elIndex = list.listitems.findIndex(item => item._id == listitem._id);
    // Update
    if (elIndex > -1) {
      // Find the todo and swap it out.
      list.listitems.splice(elIndex, 1, listitem);
    } else {
      // Insert
      list.listitems.push(listitem);
    }
  },
  // Add a todo
  listitem_delete(state, listitem) {
    // Try to find the todo list that the todo belongs to.
    let list = state.member_lists.find(list => list._id == listitem.list);
    if (!list) return;

    let elIndex = list.listitems.findIndex(item => item._id == listitem._id);
    if (elIndex > -1) {
      list.listitems.splice(elIndex, 1);
    }
  },

  // List upsert
  list_upsert(state, list) {
    let elIndex = state.member_lists.findIndex(storedlist => storedlist._id == list._id);
    // If we find a list, update it
    if (elIndex > -1) {
      state.member_lists.splice(elIndex, 1, list);
    } else { // Else, add it
      state.member_lists.push(list);
    }
  },
  // List delete
  list_delete(state, list) {
    let elIndex = state.member_lists.findIndex(storedlist => storedlist._id == list._id);
    // If we find a list, update it
    if (elIndex > -1) {
      state.member_lists.splice(elIndex, 1);
    }
  },

  // Calendar upsert
  calendar_upsert(state, calendar) {
    let elIndex = state.member_calendars.findIndex(cal => cal._id == calendar._id);
    // update
    if (elIndex > -1) {
      state.member_calendars.splice(elIndex, 1, calendar);
    } else { // Insert
      state.member_calendars.push(calendar);
    }
  },
  // Calendar delete
  calendar_delete(state, calendar) {
    let elIndex = state.member_calendars.findIndex(cal => cal._id == calendar._id);
    if (elIndex > -1) {
      state.member_calendars.splice(elIndex, 1);
    }
  },

  // Update a calendaritem
  calendaritem_upsert(state, calendaritem) {
    // Try to find the calendar
    let calendar = state.member_calendars.find(calendar => calendar._id == calendaritem.calendar);
    if (!calendar) return;

    let elIndex = calendar.calendaritems.findIndex(item => item._id == calendaritem._id);

    // Update
    if (elIndex > -1) {
      // Find the item and swap it
      calendar.calendaritems.splice(elIndex, 1, calendaritem);
    } else {
      // Insert
      calendar.calendaritems.push(calendaritem);
    }
  },
  // Update a calendaritem
  calendaritem_delete(state, calendaritem) {
    // Try to find the calendar
    let calendar = state.member_calendars.find(calendar => calendar._id == calendaritem.calendar);
    if (!calendar) return;

    let elIndex = calendar.calendaritems.findIndex(item => item._id == calendaritem._id);

    if (elIndex > -1) {
      calendar.calendaritems.splice(elIndex, 1);
    }
  },

  // Update note
  note_upsert(state, note) {
    let notelist = state.member_notelists.find(list => list._id == note.notelist);
    if (!notelist) return;

    let elIndex = notelist.notes.findIndex(item => item._id == note._id);

    // Update
    if (elIndex > -1) {
      notelist.notes.splice(elIndex, 1, note);
    } else {
      notelist.notes.push(note);
    }

  }
}










/** ------------------------------------
 * Actions
------------------------------------- */
export const actions = {
  // Update user and token
  updateUserAndToken({ commit }, payload) {
    commit('updateUser', payload.user);
    if (process.browser) {
      localStorage.setItem('authToken', payload.jwt);
    }
  },

  // Fetch user details from localStorage/server
  fetchUserDetails({ commit, dispatch }) {
    // console.log('okay');
    if (process.browser) {
      // See if user details are in localStorage, store them if they are
      let user = localStorage.getItem('user');
      if (user) {
        commit('updateUser', JSON.parse(user));
      }
      // Then check for authToken. Validate it - if fail, remove user data.
      let authToken = localStorage.getItem('authToken');
      if (!authToken) {
        commit('updateUser', null);
        return;
      }

      return this.$axios
        .get('/users/me')
        .then(res => res.data)
        .then(user => {
          commit('updateUser', user);
          dispatch('fetchCalendars');
          dispatch('fetchLists');
          return true;
        })
        .catch(err => {
          // console.log(err);
          commit('updateUser', null);
          localStorage.removeItem('authToken');
          localStorage.removeItem('user');
        })
    }
  }, // End fetchUserDetails

  // Joining/leaving socket rooms.
  joinSocketRoom({ dispatch }, room) {
    if (this.$socket) this.$socket.emit("join", room);
  },
  leaveSocketRoom({ dispatch }, room) {
    if (this.$socket) this.$socket.emit("leave", room);
  },

  // Fetch todo-lists
  fetchTodoLists({ commit }) {
    return this.$axios
      .get('/todolists/users-lists')
      .then(res => res.data)
      .then(data => {
        commit('updateStateProperty', {property: 'member_todolists', value: data.member_todolists});
      })
  },

  // Fetch calendars
  fetchCalendars({ commit }) {
    return this.$axios
      .get('/calendars/users-calendars')
      .then(res => res.data)
      .then(data => {
        commit('updateStateProperty', {property: 'member_calendars', value: data.member_calendars});
      })
  },

  // Fetch lists
  async fetchLists({ commit }) {
    let lists = await this.$axios.get('/lists/mine')
        .then(res => res.data);
    if (lists) {
      commit('updateStateProperty', {
        property: 'member_lists',
        value: lists
      });
    }
  }
}